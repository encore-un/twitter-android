package worldline.ssm.rd.ux.wltwitter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

public class TweetsAdapter extends BaseAdapter {
    private List<Tweet> tweets;
    private LayoutInflater inflater;
    private Context context;

    public TweetsAdapter(List<Tweet> tweets, Context context) {
        this.tweets = tweets;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return null != tweets ? tweets.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != tweets ? tweets.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.tweet, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Tweet tweet = (Tweet) getItem(position);

        holder.name.setText(tweet.user.name);
        holder.alias.setText(tweet.user.screenName);
        holder.text.setText(tweet.text);

        holder.button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(inflater.getContext(), "Retweeted", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    class ViewHolder {
        // public ImageView image;
        public TextView name;
        public TextView alias;
        public TextView text;
        public Button button;

        public ViewHolder(View view) {
            // image = (ImageView) view.findViewById(R.id.tweet_image);
            name = (TextView) view.findViewById(R.id.tweet_name);
            alias = (TextView) view.findViewById(R.id.tweet_alias);
            text = (TextView) view.findViewById(R.id.tweet_content);
            button = (Button) view.findViewById(R.id.tweet_button);
        }
    }
}
