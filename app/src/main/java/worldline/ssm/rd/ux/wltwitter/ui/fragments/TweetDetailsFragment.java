package worldline.ssm.rd.ux.wltwitter.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.R;

public class TweetDetailsFragment extends Fragment implements View.OnClickListener {
    public TweetDetailsFragment() {
        // Required empty public constructor
    }

    public static TweetDetailsFragment newInstance(String content, String username, String alias, String profileImageUrl) {
        final TweetDetailsFragment fragment = new TweetDetailsFragment();
        Bundle args = new Bundle();
        args.putString("content", content);
        args.putString("username", username);
        args.putString("alias", alias);
        args.putString("imageUrl", profileImageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String content = getArguments().getString("content");
        String username = getArguments().getString("username");
        String alias = getArguments().getString("alias");
        String profileImageUrl = getArguments().getString("imageUrl");

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tweet_details, container, false);

        TextView contentView = (TextView) rootView.findViewById(R.id.contentTweetDetail);
        TextView usernameView = (TextView) rootView.findViewById(R.id.usernameTweetDetail);
        TextView aliasView = (TextView) rootView.findViewById(R.id.aliasTweetDetail);

        usernameView.setText(username);
        contentView.setText(content);
        aliasView.setText(alias);

        Button retweetButton = (Button) rootView.findViewById(R.id.retweetButton);
        Button replyButton = (Button) rootView.findViewById(R.id.replyTweetButton);
        Button starButton = (Button) rootView.findViewById(R.id.starTweetButton);

        replyButton.setOnClickListener(this);
        retweetButton.setOnClickListener(this);
        starButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retweetButton: {
                Toast.makeText(getActivity(), "Retweeted", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.replyTweetButton: {
                Toast.makeText(getActivity(), "Replied", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.starTweetButton: {
                Toast.makeText(getActivity(), "Starred", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
