package worldline.ssm.rd.ux.wltwitter.async;

import android.os.AsyncTask;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetChangeListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

public class RetrieveTweetsAsyncTask extends AsyncTask<String, Void, List<Tweet>> {
    private TweetChangeListener tweetListener;

    public RetrieveTweetsAsyncTask(TweetChangeListener listener) {
        tweetListener = listener;
    }

    @Override
    protected List<Tweet> doInBackground(String... params) {
        String login = params[0];

        if (login == null) return null;
        else return TwitterHelper.getTweetsOfUser(login);
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        if (tweetListener != null && tweets != null)
            tweetListener.onTweetRetrieved(tweets);
    }
}
