package worldline.ssm.rd.ux.wltwitter.ui.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.TweetsAdapter;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.async.RetrieveTweetsAsyncTask;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabaseContract;
import worldline.ssm.rd.ux.wltwitter.databases.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetChangeListener;
import worldline.ssm.rd.ux.wltwitter.interfaces.TweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.utils.PreferenceUtils;


public class TweetsFragment extends Fragment implements TweetChangeListener, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    private ListView listView;
    private TweetListener tweetListener;

    public TweetsFragment() {
        // empty constructor
    }

    @Override
    public void onStart(){
        super.onStart();
        //getLoaderManager().initLoader(0, null, this); // TODO : NullPointerException on getContext()??
        final String login = PreferenceUtils.getLogin(getActivity().getApplicationContext());

        if (!TextUtils.isEmpty(login)) {
            RetrieveTweetsAsyncTask asyncTask = new RetrieveTweetsAsyncTask(this);
            asyncTask.execute(login);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView) rootView.findViewById(R.id.tweetsListViewer);
        listView.setOnItemClickListener(this);

        final ProgressBar progressBar = new ProgressBar(getActivity());
        progressBar.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        progressBar.setIndeterminate(true);
        listView.setEmptyView(progressBar);

        ViewGroup root = (ViewGroup) rootView.findViewById(R.id.tweetsRootRelativeLayout);
        root.addView(progressBar);

        return rootView;
    }

    @Override
    public void onTweetRetrieved(List<Tweet> tweets) {
        WLTwitterDatabaseManager manager = new WLTwitterDatabaseManager();
        manager.testDatabase(tweets, this.getActivity());
        final TweetsAdapter adapter = new TweetsAdapter(tweets, getActivity());
        listView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        if (tweetListener == null)
            return;

        final Tweet tweet = (Tweet)adapter.getItemAtPosition(position);
        tweetListener.onViewTweet(tweet);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof TweetListener) {
            tweetListener = (TweetListener) activity;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final CursorLoader cursorLoader = new CursorLoader(WLTwitterApplication.getContext());
        cursorLoader.setUri(WLTwitterDatabaseContract.TWEETS_URI);
        cursorLoader.setProjection(WLTwitterDatabaseContract.PROJECTION_FULL);
        cursorLoader.setSelection(null);
        cursorLoader.setSelectionArgs(null);
        cursorLoader.setSortOrder(null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (null != data) {
            while (data.moveToNext()) {
                final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(data);
                Log.d("TweetsFragment", tweet.toString());
            }

            if (!data.isClosed()) {
                data.close();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
