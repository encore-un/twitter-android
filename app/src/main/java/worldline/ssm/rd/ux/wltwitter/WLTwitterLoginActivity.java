package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.utils.Constants;

public class WLTwitterLoginActivity extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences prefs = this.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String login = prefs.getString("key_login", null);
        String password = prefs.getString("key_password", null);

        if (login == null || password == null)
            return;

        Intent twitterIntent = new Intent(this, MainActivity.class);
        Bundle extras = new Bundle();
        extras.putString("login", login);
        twitterIntent.putExtras(extras);
        startActivity(twitterIntent);

        findViewById(R.id.loginButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText loginText = (EditText) findViewById(R.id.loginEditText);
        EditText passwordText = (EditText) findViewById(R.id.passwordEditText);

        String login = loginText.getText().toString();
        String password = passwordText.getText().toString();

        if (TextUtils.isEmpty(login)) {
            Toast.makeText(this, "Error: login empty.", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Error: password empty.", Toast.LENGTH_SHORT).show();
        } else {
            SharedPreferences prefs = this.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
            prefs.edit().putString("key_login", login).apply();
            prefs.edit().putString("key_password", password).apply();

            Intent twitterIntent = new Intent(this, MainActivity.class);
            Bundle extras = new Bundle();
            extras.putString("login", login);
            twitterIntent.putExtras(extras);
            startActivity(twitterIntent);
        }
    }
}
